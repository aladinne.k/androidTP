package com.example.octanet.demo;

import java.io.Serializable;

public class Note implements Serializable {
    private  String nom ;
    private String note ;
    private Boolean important ;

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public Boolean getImportant() {
        return important;
    }

    public void setImportant(Boolean important) {
        this.important = important;
    }
}
