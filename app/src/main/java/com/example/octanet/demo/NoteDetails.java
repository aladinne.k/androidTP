package com.example.octanet.demo;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

public class NoteDetails extends AppCompatActivity {

    TextView tv ;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_note_details);
        Intent intent = getIntent();
        Note note = (Note) intent.getSerializableExtra("note");
        tv = (TextView) findViewById(R.id.textView3) ;
        tv.setText(note.getNote()+" - "+note.getNom());
    }
}
