package com.example.octanet.demo;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.util.Log;

public class RestApiGet {

    private static final String OPEN_WEATHER_MAP_API =
            "https://jsonplaceholder.typicode.com/posts/42";

    public static JSONObject getJSON(){
        try {
            URL url = new URL(String.format(OPEN_WEATHER_MAP_API));
            HttpURLConnection connection =
                    (HttpURLConnection)url.openConnection();



            BufferedReader reader = new BufferedReader(
                    new InputStreamReader(connection.getInputStream()));

            StringBuffer json = new StringBuffer(1024);
            String tmp="";
            while((tmp=reader.readLine())!=null)
                json.append(tmp).append("\n");
            reader.close();

            JSONObject data = new JSONObject(json.toString());

            // This value will be 404 if the request was not
            // successful

            return data;
        }catch(Exception e){
            JSONObject j = new JSONObject();
            try {
                j.put("log",e.getMessage());
            } catch (JSONException e1) {
                e1.printStackTrace();
            }
            return j ;
        }
    }
}