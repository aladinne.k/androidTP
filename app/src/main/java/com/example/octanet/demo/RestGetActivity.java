package com.example.octanet.demo;

import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.EditText;

import org.json.JSONException;
import org.json.JSONObject;

public class RestGetActivity extends AppCompatActivity {
    public Handler handler ;
    EditText dataTxt ;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rest_get);
        handler = new Handler();
        dataTxt = (EditText) findViewById(R.id.editText2);


        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);


        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });






        Thread threadToGetData  = new Thread(new Runnable() {
            @Override
            public void run() {
                final JSONObject json = RestApiGet.getJSON();
                if(json == null){
                    handler.post(new Runnable(){
                        public void run(){
                            dataTxt.setText("data not found");

                        }
                    });
                } else {
                    handler.post(new Runnable(){
                        public void run(){
                            try {
                                dataTxt.setText(json.toString(2));
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    });
                }
            }
        });


        threadToGetData.start();






    }

}
