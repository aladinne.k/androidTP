package com.example.octanet.demo;


import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

public class NoteArrayAdapter extends ArrayAdapter<Note> {
    private static final String TAG = "NoteArrayAdapter";
    private List<Note> noteList = new ArrayList<Note>();

    static class NoteViewHolder {

        TextView name;
        TextView note;
    }

    public NoteArrayAdapter (Context context, int textViewResourceId) {
        super(context, textViewResourceId);
    }

    @Override
    public void add(Note object) {
        noteList.add(object);
        super.add(object);
    }

    @Override
    public int getCount() {
        return this.noteList.size();
    }

    @Override
    public Note getItem(int index) {
        return this.noteList.get(index);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View row = convertView;
        NoteViewHolder viewHolder;
        if (row == null) {
            LayoutInflater inflater = (LayoutInflater) this.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            row = inflater.inflate(R.layout.listview_row_layout, parent, false);
            viewHolder = new NoteViewHolder();
            viewHolder.name = (TextView) row.findViewById(R.id.name);
            viewHolder.note = (TextView) row.findViewById(R.id.note);

            row.setTag(viewHolder);
        } else {
            viewHolder = (NoteViewHolder)row.getTag();
        }
        Note note = getItem(position);

        viewHolder.name.setText(note.getNom());

        viewHolder.note.setText(note.getNote());
        if(note.getNom()!="") {
            viewHolder.name.setBackgroundColor(Color.parseColor("#000000"));

        }else{
            viewHolder.name.setBackgroundColor(Color.parseColor("#ffffff"));

        }
        return row;
    }

    public Bitmap decodeToBitmap(byte[] decodedByte) {
        return BitmapFactory.decodeByteArray(decodedByte, 0, decodedByte.length);
    }
}