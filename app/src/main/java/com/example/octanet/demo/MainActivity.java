package com.example.octanet.demo;

import android.content.Intent;
import android.os.Parcelable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class MainActivity extends AppCompatActivity {
    private Button addNoteBtn ;
    private EditText noteTxt ;

    private String[]notes=new String[]{};
    private ListView mainListView ;
    private ArrayAdapter<String> listAdapter ;



    private Button getDatabtn ;
    private NoteArrayAdapter noteArrayAdapter;
    private ListView listView;

    private static int colorIndex;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        addNoteBtn= (Button) findViewById(R.id.addNoteBtn);
        noteTxt = (EditText) findViewById(R.id.note);
        mainListView = (ListView) findViewById(R.id.lv);
        getDatabtn = (Button) findViewById(R.id.getdatabtn);
//        String[] notes = new String[] {};
//        ArrayList<String> notesList = new ArrayList<String>();
//        notesList.addAll( Arrays.asList(notes) );

//        listAdapter = new ArrayAdapter<String>(this, R.layout.simplerow, notesList);
//
//        mainListView.setAdapter( listAdapter );
//        final ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, R.layout.activity_main,notes);


        listView = (ListView) findViewById(R.id.lv);
        noteArrayAdapter  = new NoteArrayAdapter(getApplicationContext(), R.layout.listview_row_layout);
        listView.setAdapter(noteArrayAdapter);








        addNoteBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

//                listAdapter.add(noteTxt.getText()+"");
//                mainListView.setAdapter( listAdapter );
                Note note = new Note();
                note.setNom("test");
                note.setNote(noteTxt.getText()+"");

                noteArrayAdapter.add(note);
                Toast toast = Toast.makeText(getApplicationContext(),noteTxt.getText(),Toast.LENGTH_LONG);
                toast.show();

            }
        });




        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position,
                                    long id) {
                Note note = (Note) parent.getItemAtPosition(position);
                Toast toast = Toast.makeText(getApplicationContext(),note.getNote()+" "+note.getNom(),Toast.LENGTH_LONG);
                //toast.show();
                Intent intent = new Intent(MainActivity.this, NoteDetails.class);
                intent.putExtra("note",note);
                startActivity(intent);
            }
        });


        getDatabtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, RestGetActivity.class);

                startActivity(intent);
            }
        });



    }

}
